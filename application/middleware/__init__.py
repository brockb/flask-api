from .auth import AuthorizationMiddleware

middleware = [AuthorizationMiddleware()]