from flask import request
from ..utils import decode_token
from .. import app

class AuthorizationMiddleware(object):
  def resolve(self, next, root, info, **args):
    token = request.headers.get("Authorization")
    
    try:
      payload = decode_token(token.replace("Bearer ", ""))
      info.context.authorized = (payload != None or app.config.get('ENV') == 'development')
    except Exception as e:
      info.context.authorized = app.config.get('ENV') == 'development'
    
    return next(root, info, **args)