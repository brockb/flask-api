import graphene

from .auth import AuthMutation

class RootMutation(graphene.ObjectType):
  """
    Class for consolidating all mutations to be used in our schema
  """
  create_user = AuthMutation.Field()