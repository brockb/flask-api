import graphene
import pymongo
import bcrypt

from ...models import AuthModel
from ...utils import generate_token, mongo_client
from typing import Dict

class AuthMutation(graphene.Mutation, AuthModel):
  """
    Class in charge of user creation
  """
  class Arguments(object):
    """
      Arguments for creating a new user

      username is stored as _id
      password is hashed and stored as password
    """
    username = graphene.String(required=True)
    password = graphene.String(required=True)

  def mutate(root, info, username: str, password: str) -> Dict:
    """
      mutation that saves new user to database

      raises ValueError if a user with specified username already exists
      returns a jwt with username as the payload
    """
    hashed_pwd = bcrypt.hashpw(password.encode(), bcrypt.gensalt())

    try:
      mongo_client.authentication.users.insert_one({
        "_id": username,
        "password": hashed_pwd
      })
    except pymongo.errors.DuplicateKeyError as e:
      raise ValueError("USER_EXISTS") 

    token = generate_token({"sub": username})

    return {
      "username": username,
      "token": token
    }