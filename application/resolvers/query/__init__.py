from .auth import AuthQuery

class RootQuery(AuthQuery):
  """
    Class for consolidating all queries to be used in our schema
  """
  pass