import graphene
import bcrypt

from ...models import AuthModel
from ...utils import generate_token, mongo_client
from typing import Dict

class AuthQuery(graphene.ObjectType):
  """
    Class for handling logins
  """
  login = graphene.Field(
    AuthModel,
    username=graphene.String(required=True),
    password=graphene.String(required=True)
  )

  def resolve_login(self, info, username: str, password: str) -> Dict:
    """
      login resolver

      raises ValueError if the user is not found or if an incorrect
      password is entered
    """
    user = mongo_client.authentication.users.find_one({ "_id": username })

    if user is None:
      raise ValueError("INVALID_USER")

    hashed_pwd = user.get("password")
    if not bcrypt.checkpw(password.encode(), hashed_pwd):
      raise ValueError("INVALID_PWD")

    token = generate_token({"sub": username})

    return {
      "username": username,
      "token": token 
    }