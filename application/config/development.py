from .common import Config

class DevelopmentConfig(Config):
  ENV = 'development'
  HTTP_METHODS = ['POST', 'GET']
  SECRET_KEY = "super secret key"
  GRAPHIQL = True
  DEBUG = True
  DB_SERVER = 'localhost'
  DB_PORT = 27017