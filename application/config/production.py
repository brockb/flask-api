from .common import Config

import os

class ProductionConfig(Config):
  ENV = 'production'
  HTTP_METHODS = ['POST']
  SECRET_KEY = os.getenv('SECRET_KEY')
  GRAPHIQL = False
  DEBUG = False
  DB_SERVER = os.getenv('DB_SERVER')
  DB_PORT = None