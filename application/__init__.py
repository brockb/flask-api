import os

from flask import Flask
from .config import ProductionConfig, DevelopmentConfig

if os.getenv('FLASK_MODE') == 'production':
  active_config = ProductionConfig()
else: 
  active_config = DevelopmentConfig()

app = Flask(__name__)
app.config.from_object(active_config)