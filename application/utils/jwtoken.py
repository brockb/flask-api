import jwt

from datetime import datetime, timedelta
from typing import Dict
from .. import app

def generate_token(payload: Dict) -> str:
  """
    generates a json web token based on the secret key using the 
    provided algorithm
  """
  payload.setdefault('exp', datetime.utcnow() + timedelta(hours=1))
  payload.setdefault('iat', datetime.utcnow())
  
  return jwt.encode(payload, app.config.get("SECRET_KEY"), algorithm='HS256').decode('utf-8')

def decode_token(token: str) -> Dict:
  """
    decode a json web token generated on this server, and
    returns the payload
  """
  try:
    payload = jwt.decode(token, app.config.get("SECRET_KEY"), algorithms=['HS256'])
  except jwt.exceptions.DecodeError as identifier:
    return None
    
  return payload