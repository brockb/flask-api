import pymongo

from .. import app

mongo_client = pymongo.MongoClient(
  host=app.config.get("DB_SERVER"),
  port=app.config.get("DB_PORT")
)