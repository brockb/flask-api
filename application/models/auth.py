import graphene

class AuthModel(graphene.ObjectType):
  """
    Data that is returned on login and user creation
  """
  username = graphene.String()
  token = graphene.String()