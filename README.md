# Flask-api

Python database API written for mongoDB.

## Requirements

python 3.8 or later

## Installation

This project uses pipenv to manage dependencies and virtual environments:

* git clone {project url}
* cd {project directory}
* pipenv shell
* pipenv install

## Usage (for development only)

From inside the virtual environment run python main.py.
By default this will run the application in development mode, and attempt to 
connect to a local mongod running on the default port

## TODO

Requirements TBD