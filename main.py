import graphene
import application.resolvers as resolvers

from flask import request
from flask_graphql import GraphQLView
from application import app
from application.middleware import middleware

@app.route('/graphql', methods=app.config.get("HTTP_METHODS"))
def graphql():
  """
    Singular graphql endpoint. This function allows the view_func to
    handle the request based on the schema
  """
  schema = graphene.Schema(query=resolvers.RootQuery, mutation=resolvers.RootMutation)
  return GraphQLView.as_view('graphql', schema=schema, graphiql=app.config.get("GRAPHIQL"), middleware=middleware)()

if __name__ == "__main__":
  app.run(host='localhost', port=5001)